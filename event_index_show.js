(function () {
  "use strict";

  function fetchRecords(appId, opt_offset, opt_limit, opt_records) {
    var offset = opt_offset || 0;
    var limit = opt_limit || 100;
    var allRecords = opt_records || [];
    var query = "";
    query += "limit " + limit + " offset " + offset;

    var params = { app: appId, query: query };
    return kintone.api("/k/v1/records", "GET", params).then(function (resp) {
      allRecords = allRecords.concat(resp.records);
      if (resp.records.length === limit) {
        return fetchRecords(appId, offset + limit, limit, allRecords);
      }
      return allRecords;
    });
  }

  kintone.events.on(["app.record.detail.show", "app.record.index.show"], function (event) {
    // fetchRecords(_file_app_config.app_id).then(function (records) {
    //   records.forEach((record) => {
    //     if (record.is_checked.value === _work_app_config.fieldsValue.CONFIG_EMAIL_SELECT_GROUP_VALUE_SELECTED) {
    //       _file_app_config.record_id = record.$id.value;
    //     }
    //   });
    // });
    // return event;
  });

  kintone.events.on("app.record.index.show", function (event) {
    initStatusEmailUI(event);

    initSelectBoxUI(event);
    return event;
  });

  function initStatusEmailUI(event) {
    var emailListTags = kintone.app.getFieldElements("send_email_status");
    //Change the properties of the retrieved field elements for each record
    for (var i = 0; i < emailListTags.length; i++) {
      var record = event.records[i];
      if (document.getElementById("send_email_status_" + record.$id.value) != null) {
        return event;
      }

      var box = document.createElement("div");
      box.style = "text-align: center;";

      var emailTextInput = document.createElement("label");
      emailTextInput.id = "send_email_status_" + record.$id.value;
      // emailTextInput.type = "text";
      if (record.send_email_status.value == _work_app_config.fieldsValue.CONFIG_EMAIL_SENDING_STATUS_VALUE_FINISH) {
        emailTextInput.innerHTML = _work_app_config.fieldsValue.CONFIG_EMAIL_SENDING_STATUS_VALUE_FINISH;
      } else {
        emailTextInput.innerHTML = _work_app_config.fieldsValue.CONFIG_EMAIL_SENDING_STATUS_VALUE_DEFAULT;
      }
    
      box.appendChild(emailTextInput);
      emailListTags[i].innerHTML = "";
      emailListTags[i].appendChild(box);
    }
  }

  function initSelectBoxUI(event) {
    var checkRecords = kintone.app.getFieldElements(_work_app_config.fieldsCode.CONFIG_EMAIL_SELECT_GROUP_KEY);
    //Change the properties of the retrieved field elements for each record
    for (var i = 0; i < checkRecords.length; i++) {
      var record = event.records[i];
      if (document.getElementById("template_record_id_" + i) != null) {
        return event;
      }

      var box = document.createElement("div");
      box.style = "text-align: center;";
      var checkbox = document.createElement("input");
      checkbox.id = "template_record_id_" + record.$id.value;
      checkbox.name = "template_record_id";
      checkbox.value = record.$id.value;
      checkbox.type = "checkbox";
      if (record.is_checked.value == _work_app_config.fieldsValue.CONFIG_EMAIL_SELECT_GROUP_VALUE_SELECTED) {
        checkbox.checked = true;
      }
      checkbox.onclick = function () {
        var radioSefl = this;
        var updateRecordData = {};

        updateRecordData[_work_app_config.fieldsCode.CONFIG_EMAIL_SELECT_GROUP_KEY] = {
          value: _work_app_config.fieldsValue.CONFIG_EMAIL_SELECT_GROUP_VALUE_SELECTED,
        };

        var updateRecordData = {};
        var value = radioSefl.checked ? _work_app_config.fieldsValue.CONFIG_EMAIL_SELECT_GROUP_VALUE_SELECTED : _work_app_config.fieldsValue.CONFIG_EMAIL_SELECT_GROUP_VALUE_UNSELECTED;
        updateRecordData[_work_app_config.fieldsCode.CONFIG_EMAIL_SELECT_GROUP_KEY] = {
          value: value,
        };

        kintone.api(kintone.api.url("/k/v1/record", true), "PUT", {
          app: kintone.app.getId(),
          id: radioSefl.value,
          record: updateRecordData,
        });
      };

      box.appendChild(checkbox);
      checkRecords[i].innerHTML = "";
      checkRecords[i].appendChild(box);
    }
  }
})();
