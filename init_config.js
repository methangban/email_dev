const GMAIL_APP_CLIENT_ID = "847478067421-a4u3d3r9rp7dgt8hal6o2n58806ef0gm.apps.googleusercontent.com";
const APP_URL_ROOT = "https://liberal.cybozu.com/k/";

var _history_app_config = _history_app_config || {
  api_token: "vpz968EtQAB9iwODtvKifovzQFavE2lWuNiUSzEZ",
  app_id: 647,
};

var _file_app_config = _file_app_config || {
  api_token: "QTrd1TzQbBOIFEujj4zF5JBjNmwLgw7fYxWfiLCJ",
  app_id: 638,

};

var _work_app_config = _work_app_config || {
  api_token: "Zu4MhSx39xwqQcyfV7CEsThgNlpi73FG2El1EZ3M",
  app_id: 630,
  fieldsCode: {
    CONFIG_EMAIL_SENDER_KEY: "sender",
    CONFIG_EMAIL_RECIPIENTS_KEY: "recipients",
    CONFIG_EMAIL_RECIPIENTS_OTHER_KEY: "other_recipients",
    CONFIG_EMAIL_BCC_KEY: "bcc",
    CONFIG_EMAIL_CC_KEY: "cc",
    CONFIG_COMPANY_KEY: "company_name",
    CONFIG_RECRUITER_KEY: "recruiter_name",
    CONFIG_RECRUITER_DEPART_KEY: "recruiter_department_name",
    CONFIG_DEPARTMENT_KEY: "department_name",
    CONFIG_SENDER_NAME_KEY: "company_caller",

    CONFIG_EMAIL_SELECT_RECIPIENT_KEY: "select_email",// 必須
    CONFIG_EMAIL_SENDING_STATUS_KEY: "send_email_status", // 必須
    CONFIG_EMAIL_SELECT_GROUP_KEY: "is_checked"// 必須
  },
  fieldsValue: {
    CONFIG_EMAIL_SENDER_DEFAULT_VALUE: "会社メールアドレス",// 必須
    CONFIG_EMAIL_SENDING_STATUS_VALUE_FINISH : "送信済み",// 必須
    CONFIG_EMAIL_SENDING_STATUS_VALUE_DEFAULT :"未送信",// 必須
    CONFIG_EMAIL_SELECT_GROUP_VALUE_UNSELECTED:"選択しない",// 必須
    CONFIG_EMAIL_SELECT_GROUP_VALUE_SELECTED : "選択する"// 必須
  },
  temFieldsCode: {
    CONFIG_COMPANY_NAME : "company_name",
    CONFIG_RECRUITER_NAME: "recruiter_name",
    CONFIG_DEPARTMENT_NAME : "recruiter_department_name",
    CONFIG_REC_DEPART_NAME : "department_name",
    CONFIG_CALLER : "company_caller"
  }
};