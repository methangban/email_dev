kintone.events.on("app.record.index.show", function (event) {
  var body = {
    app: 657,
    records: [
      {
        Text: {
          value: "Sample001",
        },
        Number: {
          value: 1,
        },
      },
      {
        Text: {
          value: "Sample002",
        },
        Number: {
          value: 2,
        },
      },
    ],
  };

  kintone.api(
    kintone.api.url("/k/v1/records", true),
    "POST",
    body,
    function (resp) {
      // success
      console.log(resp);
    },
    function (error) {
      // error
      console.log(error);
    }
  );
});
