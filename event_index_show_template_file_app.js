(function (PLUGIN_ID, KC, GAPI) {
  "use strict";

  function fetchRecords(appId, opt_offset, opt_limit, opt_records) {
    var offset = opt_offset || 0;
    var limit = opt_limit || 100;
    var allRecords = opt_records || [];
    var params = { app: appId, query: "limit " + limit + " offset " + offset };
    return kintone.api("/k/v1/records", "GET", params).then(function (resp) {
      allRecords = allRecords.concat(resp.records);
      if (resp.records.length === limit) {
        return fetchRecords(appId, offset + limit, limit, allRecords);
      }
      return allRecords;
    });
  }

  // kintone.events.on("app.record.detail.show", function (event) {
  //   fetchRecords(kintone.app.getId()).then(function (records) {
  //     records.forEach((record) => {
  //       if (
  //         /*record.app_type.value === _work_app_config.app_name &&*/
  //         record.is_checked.value === "選択する"
  //       ) {
  //         _file_app_config.record_id = record.$id.value;
  //       }
  //     });
  //   });
  //   // return event;
  // });

  kintone.events.on("app.record.index.show", function (event) {
    //Retrieve an array of field elements of the fields with field code of "lead_status"
    var checkRecords = kintone.app.getFieldElements("is_checked");
    //Change the properties of the retrieved field elements for each record
    for (var i = 0; i < checkRecords.length; i++) {
      var record = event.records[i];
      if (document.getElementById("template_record_id_" + i) != null) {
        return event;
      }

      var box = document.createElement("div");
      box.style = "text-align: center;";
      var checkbox = document.createElement("input");
      checkbox.id = "template_record_id_" + record.$id.value;
      checkbox.name = "template_record_id";
      checkbox.value = record.$id.value;
      checkbox.type = "radio";
      if (record.is_checked.value == "選択する") {
        checkbox.checked = true;
      }
      checkbox.onclick = function () {
        var radioSefl = this;
        var updateRecordData = {};

        updateRecordData["is_checked"] = {
          value: "選択する",
        };
        var recordId = radioSefl.value;
        console.log(recordId);

        var recordPromises = [];
        event.records.map((record) => {
          var value = recordId != record.$id.value ? "選択しない" : "選択する";

          var updateRecordData = {};

          updateRecordData["is_checked"] = {
            value: value,
          };
          var promise = kintone.api(kintone.api.url("/k/v1/record", true), "PUT", {
            app: kintone.app.getId(),
            id: record.$id.value,
            record: updateRecordData,
          });
          recordPromises.push(promise);
        });
        Promise.all(recordPromises).then((values) => {
          // location.reload();
          console.log("done...");
        });
      };

      box.appendChild(checkbox);
      checkRecords[i].innerHTML = "";
      checkRecords[i].appendChild(box);
    }
    return event;
  });

  kintone.events.on("app.record.index.edit.submit", function (event) {
    var recordId = event.record.$id.value;
    console.log(event.record.is_checked.value);

    fetchRecords(kintone.app.getId()).then(function (records) {
      var recordPromises = [];
      records.map((record) => {
        if (event.record.is_checked.value == "選択する") {
          if (recordId == record.$id.value) {
            // record.style.color = "blue";
          } else {
            if (record.is_checked.value == "選択する") {
              record.is_checked.value = "選択しない";
              var updateRecordData = {};

              updateRecordData["is_checked"] = {
                value: "選択しない",
              };
              var promise = kintone.api(kintone.api.url("/k/v1/record", true), "PUT", {
                app: kintone.app.getId(),
                id: record.$id.value,
                record: updateRecordData,
              });
              recordPromises.push(promise);
            }
          }
        }
      });

      Promise.all(recordPromises).then((values) => {
        location.reload();
      });
    });

    return event;
  });

})();
